Pod::Spec.new do |spec|
  spec.name     = 'CarnegieSimpleLog'
  spec.version  = '0.0.1'
  spec.author   = 'Carnegie Technologies'
  spec.license  = 'Commercial'
  spec.homepage = 'https://www.carnegietechnologies.com/'
  spec.source   = { :git => 'https://gitlab.com/dma-carnegie/simplelog.git',
                    :tag => "#{spec.version}" }
  spec.summary  = 'Simple logging module'
  spec.platform = :ios, '7.0'

  spec.source_files        = 'simplelog/include/*.h'
  spec.public_header_files = 'simplelog/include/*.h'
  spec.header_dir          = 'SimpleLog'
  spec.vendored_libraries  = 'simplelog/lib/libIosSimpleLog.a'
  spec.preserve_paths      = 'simplelog/lib/*.a'
end
